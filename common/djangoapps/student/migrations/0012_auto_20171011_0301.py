# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0011_auto_20171010_2053'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='kotama',
            field=models.CharField(blank=True, max_length=255, db_index=True, choices=[(b'1', b'KODAM I/BB'), (b'2', b'KODAM II/SWJ'), (b'3', b'KODAM III/SLW'), (b'4', b'KODAM IV/DIP'), (b'5', b'KODAM V/BRW'), (b'6', b'KODAM VI/MLW'), (b'7', b'KODAM VII/WRB'), (b'9', b'KODAM IX/UDY'), (b'10', b'KODAM IM'), (b'12', b'KODAM XII/TPR'), (b'15', b'KODAM JAYA'), (b'16', b'KODAM XVI/PTM'), (b'17', b'KODAM XVII/CEN'), (b'18', b'KOPASSUS'), (b'19', b'KOSTRAD'), (b'20', b'KODIKLAT TNI AD'), (b'21', b'DITZIAD'), (b'22', b'DITBEKANGAD'), (b'23', b'DITPALAD')]),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='satminkal',
            field=models.CharField(blank=True, max_length=255, db_index=True, choices=[(b'1A0A19', b'DIVIF I/PRAKASA VIRA GUPTI'), (b'1A0B1', b'YONIF 100/RAIDER'), (b'1A0B2', b'YONIF 141 BS/ANEKA YUDHA J P'), (b'1A0B4', b'YONIF RAIDER 400/ BANTENG RAIDER'), (b'1A0B6', b'YONIF 614 /RAJA PANDHITA'), (b'1A0B14', b'YONIF 700/WYC'), (b'1A0B16', b'YONIF 733 /RAIDER'), (b'1A1A1', b'BRIGIF-7/RIMBA RAYA'), (b'1A1A3', b'BRIGIF-15/DAM III/SLW'), (b'1A1A4', b'BRIGIF-4/DEWA RATNA')]),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='gender',
            field=models.CharField(blank=True, max_length=6, null=True, db_index=True, choices=[(b'm', b'Male'), (b'f', b'Female')]),
        ),
    ]
