# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0010_auto_20171010_2027'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='alamat',
            field=models.CharField(db_index=True, max_length=255, blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='panda',
            field=models.CharField(db_index=True, max_length=255, blank=True),
        ),
    ]
