# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0013_auto_20171012_1756'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='jabatan',
            field=models.CharField(db_index=True, max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='gender',
            field=models.CharField(blank=True, max_length=6, null=True, db_index=True, choices=[(b'm', b'Laki-Laki'), (b'f', b'Perempuan')]),
        ),
    ]
