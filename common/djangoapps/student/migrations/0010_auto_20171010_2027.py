# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0009_auto_20171010_2024'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='korp',
            field=models.CharField(blank=True, max_length=255, db_index=True, choices=[(b'Inf', b'Inf'), (b'Kav', b'Kav'), (b'Arm', b'Arm'), (b'Arh', b'Arh'), (b'Czi', b'Czi'), (b'Cpm', b'Cpm'), (b'Cba', b'Cba'), (b'Ckm', b'Ckm'), (b'Cpl', b'Cpl'), (b'Chb', b'Chb'), (b'Chk', b'Chk'), (b'Caj', b'Caj'), (b'Cku', b'Cku'), (b'Ctp', b'Ctp'), (b'Cpn', b'Cpn')]),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='pangkat',
            field=models.CharField(blank=True, max_length=255, db_index=True, choices=[(b'Letda', b'Letda'), (b'Lettu', b'Lettu'), (b'Kapten', b'Kapten'), (b'Mayor', b'Mayor'), (b'Letkol', b'Letkol'), (b'Kolonel', b'Kolonel'), (b'Brigjen', b'Brigjen'), (b'Mayjen', b'Mayjen'), (b'Letjen', b'Letjen'), (b'Jenderal', b'Jenderal'), (b'Serda', b'Serda'), (b'Sertu', b'Sertu'), (b'Serka', b'Serka'), (b'Serma', b'Serma'), (b'Pelda', b'Pelda'), (b'Peltu', b'Peltu'), (b'Prada', b'Prada'), (b'Pratu', b'Pratu'), (b'Praka', b'Praka'), (b'Kopda', b'Kopda'), (b'Koptu', b'Koptu'), (b'Kopka', b'Kopka'), (b'IV/D', b'IV/D'), (b'IV/C', b'IV/C'), (b'IV/B', b'IV/B'), (b'IV/A', b'IV/A'), (b'III/D', b'III/D'), (b'III/C', b'III/C'), (b'III/B', b'III/B'), (b'III/A', b'III/A'), (b'II/D', b'II/D'), (b'II/C', b'II/C'), (b'II/B', b'II/B'), (b'II/A', b'II/A'), (b'I/D', b'I/D'), (b'I/C', b'I/C'), (b'I/B', b'I/B'), (b'I/A', b'I/A'), (b'Siswa', b'Siswa')]),
        ),
    ]
