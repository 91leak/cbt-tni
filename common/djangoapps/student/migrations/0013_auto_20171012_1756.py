# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0012_auto_20171011_0301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='kotama',
            field=models.CharField(blank=True, max_length=255, db_index=True, choices=[(b'KODAM1', b'KODAM I/BB'), (b'KODAM2', b'KODAM II/SWJ'), (b'KODAM3', b'KODAM III/SLW'), (b'KODAM4', b'KODAM IV/DIP'), (b'KODAM5', b'KODAM V/BRW'), (b'KODAM6', b'KODAM VI/MLW'), (b'KODAM7', b'KODAM VII/WRB'), (b'KODAM9', b'KODAM IX/UDY'), (b'KODAM10', b'KODAM IM'), (b'KODAM12', b'KODAM XII/TPR'), (b'KODAM15', b'KODAM JAYA'), (b'KODAM16', b'KODAM XVI/PTM'), (b'KODAM17', b'KODAM XVII/CEN'), (b'KOPASUS18', b'KOPASSUS'), (b'KOSTRAD19', b'KOSTRAD'), (b'KODIKLAT20', b'KODIKLAT TNI AD'), (b'DITZIAD21', b'DITZIAD'), (b'DITBEKANG22', b'DITBEKANGAD'), (b'DITPALAD23', b'DITPALAD')]),
        ),
    ]
