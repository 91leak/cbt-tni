# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('external_auth', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='externalauthmap',
            name='external_alamat',
            field=models.CharField(default=b'-', max_length=255, db_index=True),
        ),
        migrations.AddField(
            model_name='externalauthmap',
            name='external_notest',
            field=models.CharField(default=b'-', max_length=255, db_index=True),
        ),
        migrations.AddField(
            model_name='externalauthmap',
            name='external_nrp',
            field=models.CharField(default=b'-', max_length=255, db_index=True),
        ),
        migrations.AddField(
            model_name='externalauthmap',
            name='external_panda',
            field=models.CharField(default=b'-', max_length=255, db_index=True),
        ),
    ]
